
/*
 * Define a class for a type called CounterType. An object of this type is used to
 * count things, so it records a count that is a nonnegative whole number. Include
 * a mutator function that sets the counter to a given count given as an argument.
 * Include member functions to increase the count by one and to decrease the count
 * by one. Be sure that no member function allows the value of the counter to 
 * become negative. Also, include a member function that returns the current count
 * value and one that outputs the count. Embed your class definition in a test
 * program. (This is basically semaphore implementation).
 */

#include <iostream>

using namespace std;

class CounterType
{
    private:
        int count;
    public:
        void increment();
        void decrement();
        int getCount();
        void setCount(int newCount);
        void printCount();
};

void CounterType::increment()
{
    count++;
}

void CounterType::decrement()
{
    if (count == 0)
        cout << "[-] unable to decrement count below zero." << endl;
    else
        count--;
}

int CounterType::getCount()
{
    return count;
}

void CounterType::setCount(int newCount)
{
    if (newCount >= 0)
        count = newCount;
    else
        cout << "[-] unable to set count to negative integer." << endl;
}

void CounterType::printCount()
{
    cout << "count is " << count << endl;
}

int main()
{
    CounterType counterOne;
    counterOne.setCount(-1);
    counterOne.setCount(0);
    counterOne.decrement();
    counterOne.setCount(5);
    counterOne.printCount();
    int count = counterOne.getCount();
    counterOne.increment();
    counterOne.printCount();
    counterOne.decrement();
    counterOne.printCount();
    return 0;
}





