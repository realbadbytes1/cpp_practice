
/*
 * Workers at a particular comany have won a 7.6% pay increase
 * retroactive for six months. Write a program that takes an employee's
 * previous annual salary as input and outputs the amount of retroactive
 * pay due the employee, the new annual salary, and the new monthly salary.
 * Use a variable declaration with the modifier `const` to express the 
 * pay increase.
 */
#include <iostream>

using namespace std;

int main()
{
    const double increase = .076;
    double old_salary = 0;
    double new_salary = 0;
    double pay_due = 0;

    cout << "current base salary: $";
    cin >> old_salary;
    new_salary = old_salary + (old_salary * increase);
    pay_due = (new_salary - old_salary) / 2;
    cout << "retroactive pay due: $" << pay_due << endl;
    cout << "new annual salary:   $" << new_salary << endl;
    cout << "new monthly salary:  $" << new_salary/12 << endl;
    return 0;
}
