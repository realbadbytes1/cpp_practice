
/*
 * Define a class called Month that is an abstract data type for a month. Your class 
 * will have one member variable of type int to represent a month (1 for January, 2
 * for February, and so forth). Include all the following member functions:
 * a constructor to set the month using the first three letters of the name as three args
 * a constructor to set the month using and integer argument
 * a default constructor
 * an input function that reads the month as an integer
 * an input function that reads the month as the first three letters
 * an output function that outputs the month as an integer
 * an output function that outputs the month as the first three letters
 * a member function that returns the next month as a value of type Month
 */

#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

class Month
{
    public:
        Month();
        Month(int m);
        Month(char one, char two, char three);
        void read_month_int(int month1);
        void read_month_chars(char one, char two, char three);
        void print_month_int();
        void print_month_chars();
        Month next_month();
    private:
        int month;
};

/* default constructor */
Month::Month()
{
}

/* int constructor */
Month::Month(int m)
{
    month = m;
}

/* three char constructor */
Month::Month(char one, char two, char three)
{
    char month_string[4];
    month_string[0] = one;
    month_string[1] = two;
    month_string[2] = three;
    month_string[3] = '\0';
    if (!strcmp(month_string, "jan"))
        month = 1;
    else if (!strcmp(month_string, "feb"))
        month = 2;
    else if (!strcmp(month_string, "mar"))
        month = 3;
    else if (!strcmp(month_string, "apr"))
        month = 4;
    else if (!strcmp(month_string, "may"))
        month = 5;
    else if (!strcmp(month_string, "jun"))
        month = 6;
    else if (!strcmp(month_string, "jul"))
        month = 7;
    else if (!strcmp(month_string, "aug"))
        month = 8;
    else if (!strcmp(month_string, "sep"))
        month = 9;
    else if (!strcmp(month_string, "oct"))
        month = 10;
    else if (!strcmp(month_string, "nov"))
        month = 11;
    else if (!strcmp(month_string, "dec"))
        month = 12;
    else
        cout << "error parsing month chars" << endl;
}

void Month::read_month_int(int month1)
{
    if (month1 < 1 || month > 12)
        cout << "error parsing month int" << endl;
    else
        month = month1;
}

void Month::read_month_chars(char one, char two, char three)
{
    char month_string[4];
    month_string[0] = one;
    month_string[1] = two;
    month_string[2] = three;
    month_string[3] = '\0';
    if (!strcmp(month_string, "jan"))
        month = 1;
    else if (!strcmp(month_string, "feb"))
        month = 2;
    else if (!strcmp(month_string, "mar"))
        month = 3;
    else if (!strcmp(month_string, "apr"))
        month = 4;
    else if (!strcmp(month_string, "may"))
        month = 5;
    else if (!strcmp(month_string, "jun"))
        month = 6;
    else if (!strcmp(month_string, "jul"))
        month = 7;
    else if (!strcmp(month_string, "aug"))
        month = 8;
    else if (!strcmp(month_string, "sep"))
        month = 9;
    else if (!strcmp(month_string, "oct"))
        month = 10;
    else if (!strcmp(month_string, "nov"))
        month = 11;
    else if (!strcmp(month_string, "dec"))
        month = 12;
    else
        cout << "error parsing month chars" << endl;
}

void Month::print_month_int()
{
    cout << month << endl;
}

void Month::print_month_chars()
{
    if (month == 1)
        cout << "jan" << endl;
    else if (month == 2)
        cout << "feb" << endl;
    else if (month == 3)
        cout << "mar" << endl;
    else if (month == 4)
        cout << "apr" << endl;
    else if (month == 5)
        cout << "may" << endl;
    else if (month == 6)
        cout << "jun" << endl;
    else if (month == 7)
        cout << "jul" << endl;
    else if (month == 8)
        cout << "aug" << endl;
    else if (month == 9)
        cout << "sep" << endl;
    else if (month == 10)
        cout << "oct" << endl;
    else if (month == 11)
        cout << "nov" << endl;
    else if (month == 12)
        cout << "dec" << endl;
    else
        cout << "error parsing month int" << endl;
}

Month Month::next_month()
{
    return Month(month+1);
}

int main()
{
    Month int_month = Month(1);
    Month char_month = Month('j', 'a', 'n');
    Month default_month = Month();

    int_month.print_month_int();
    int_month.read_month_chars('a', 'p', 'r');
    int_month.print_month_int();
    int_month.read_month_int(2);
    int_month.print_month_chars();

    Month next_month = int_month.next_month();
    next_month.print_month_int();
    next_month.print_month_chars();

    return 0;
}















