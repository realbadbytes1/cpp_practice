
/* 
 * In the sport of diving, seven judges award a score between 0 and 10, where each score
 * may be a floating point value. The highest and lowest scores are thrown out and the 
 * remaining scores are added together. The sum is then multiplied by the degree of 
 * difficulty for that dive. The degree of difficulty ranges from 1.2 to 3.8 points.
 * The total is the multiplied by 0.6 to determine the diver's score.
 *
 * Write a program that inputs a degree of difficulty and seven judges' scores, and
 * outputs the overall score for that dive. The program should ensure that all inputs
 * are within allowable data ranges.
 */

#include <iostream>
#include <cstdlib>
#include <cstdio>

using namespace std;

const int NUM_JUDGES = 7;

float score_dive(float difficulty, const float scores[]);

int main()
{
    float scores[NUM_JUDGES] = {7.2, 
                                6.2, 
                                5.0, 
                                9.8, 
                                10.0, 
                                8.6,
                                8.8};
    float difficulty;
    cout << "enter difficulty: ";
    cin >> difficulty;
    cout << "dive score: " << score_dive(difficulty, scores) << endl;
    return 0;
}

float score_dive(float difficulty, const float scores[])
{
    /* check input validity */
    if (difficulty < 1.2 || difficulty > 3.8)
        exit(1);
    for (int i = 0; i < NUM_JUDGES; i++)
    {
        if ((scores[i] < 0) || (scores[i] > 10.0))
            exit(1);
    }

    /* fill temporary score array */
    float temp_scores[NUM_JUDGES];
    for (int i = 0; i < NUM_JUDGES; i++)
    {
        temp_scores[i] = scores[i];
    }

    /* find highest index, lowest index, and sum */
    float temp = temp_scores[0];
    float sum = 0;
    int low_index = 0;
    int high_index = 0;
    for (int i = 0; i < NUM_JUDGES; i++)
    {
        /* high index */
        if (temp > temp_scores[i])
            high_index = i;
        /* low index */
        if (temp < temp_scores[i])
            low_index = i;
        sum += temp_scores[i];
        temp = scores[i];
    }
    sum -= temp_scores[high_index];
    sum -= temp_scores[low_index];
    sum *= difficulty * 0.6;

    return sum;
}
