/* default constructor */
X::X() : msg(new std::string()) {}

/* another constructor */
X::X(const std::string& s) : msg(new std::string(s)) {}

/* copy constructor */
/* copies string contents from old X into new X */
X::X(const X& x) : msg(new std::string(*x.msg)) {}

/* copy assignment operator */
X& X::operator=(const X& x)
{
    /* if current class addr is not the same as passed class addr */
    /* we remove our msg, and copy the msg from passed class */
    if (this != &x) {
        delete msg;
        msg = new std::string(*x.msg);
    }

    return *this;
}

/* destructor */
/* clean up heap memory */
X::~X()
{
    delete msg;
}

/* class method to retrieve msg */
std::string X::getMsg()
{
    return *msg;
}
