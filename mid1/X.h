class X {
    public:

        /* default constructor */
        X();

        /* another constructor */
        X(const::std::string& msg);

        /* copy constructor */
        X(const X& x);

        /* copy assignment operator */
        X& operator=(const X& x);

        /* destructor */
        ~X();

        /* class method */
        std::string getMsg();

    private:
        /* private data */
        std::string* msg;
};
        
