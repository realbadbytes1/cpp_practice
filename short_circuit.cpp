#include <iostream>

using namespace std;

int main()
{
    int drivers, cars(100);
    cout << "enter drivers: " << endl;
    cin >> drivers;
    /* div by zero wont ever occur due to short-circuit boolean evaluation */
    if ( (drivers != 0) && ((cars/drivers) >= 1))
        cout << "each driver can drive at least 1 car" << endl;
    return 0;
}
