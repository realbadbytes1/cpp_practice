#include <iostream>

using namespace std;

double avg(double one, double two);
double avg(double one, double two, double three);

int main()
{
    double one = 1.0;
    double two = 2.0;
    double three = 3.0;
    cout << avg(one, two) << endl;
    cout << avg(one, two, three) << endl;
    return 0;
}

double avg(double one, double two)
{
    return (one+two)/2.0;
}

double avg(double one, double two, double three)
{
    return (one+two+three)/3.0;
}
